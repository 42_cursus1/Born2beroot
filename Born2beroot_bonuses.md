# Born2beroot - bonuses

This is the second part toward Born2beroot completion: the bonuses.
There are 3 bonuses:
- partitioning the disk so that all main directories are contained in their own partition
	-> this part needs to be completed during the initial installation
- setting up a working wordpress website, using lighttpd and mariaDB
- setting up an additional service and explain why you chose that one (cannot be apache2 or nginx). I wanted to try an FTP server

## Partitioning the disk
Chances are, you probably did that already - at least if you followed pretty much any tutorial on Born2beroot, including mine.
If not, I'm not saying it is too late to work on it, but you're on your own ... and no I won't cover how to re-partition your disk after the fact.

## Wordpress

### Preparing for wordpress : lighttpd, mariaDB

First, you want to get the latest PHP version.
In order to do that you need to install curl (« client url »), a tool to transfer data to / from a server.
`sudo apt install curl`

If you encounter a 404 error, run it from the host instead of the VM.
The reason we use this tool is that we can execute a script to add Sury’s repository – which you can more or less consider to be the official repo for PHP ! If this sentence makes no sense to you : when we execute a sudo apt command, we don’t get the packages out of thin air, they come from a repository. Some are included already, others you may need to add to your sources.
Now execute the following command, which will add the repo :
`sudo curl -sSL https://packages.sury.org/php/README.txt | sudo bash -x`

This is the script that will be executed :
``` s
#!/bin/sh
# To add this repository please do:

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

${SUDO} apt-get update
${SUDO} apt-get -y install apt-transport-https lsb-release ca-certificates curl
${SUDO} curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg
${SUDO} sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
${SUDO} apt-get update
```

Now that the repo is added, we can – finally – install PHP and some useful dependencies :
`sudo apt install php8.1`
`sudo apt install php-common php-cgi php-cli php-mysql php8.1-mbstring  php8.1-curl php8.1-xml`

Some explanations :
    • php-cgi is a powerful admin tool (cgi = common gateway interface). Anoher common tool would be php-fpm, it includes awesome stuff like fast-cgi which we’ll need. But you can also just get php-cgi and then fast-cgi, you’ll have less setting up to do
    • I added mbstring, which is the support for non-ascii characters (if you have no plan of using an asian or middle eastern langage, you don’t need it)
    • you can write all packages either as php-* or php8.1-* as it will make no difference at all. I intentionally mixed and match so you can see both, choose one and stick to it

If you want to check that you do have the latest version (8.1.8 at the time of writing this)
`php -v`

Next step : lighttpd. Apache2 comes attached to PHP, but 1) you re not allowed to use it with this project, and 2) we are supposed to use lighttpd. So let’s get rid of Apache :
`systemctl status apache2`
`sudo apt purge apache2`
`sudo apt install lighttpd`
`sudo lighttpd -v`
(should be 1.4.59 or more)

You could also use remove instead of purge, but that won’t get rid of the local config files.
`sudo systemctl start lighttpd`
`sudo systemctl enable lighttpd`
`sudo systemctl status lighttpd`

Finally, let’s add a new UFW rule fr the http service in order to allow the http port (usually port 80, but you may want to check it just in case) :
`sudo ufw allow http`
`sudo ufw status`

As we allowed a port in the VM, we want to add a new port forwarding rule in VirtualBox. However, since http default port is 80, we want to use another – non used – port on the host. I used port 8080, you can change as needed :.
host port = 8080 // guest port = 80

In order to check that lighttpd is active, you can use either one of the following URL in any web browser on your host machine. You will get to a « placeholder page » :
http://127.0.0.1:8080
http://localhost:8080

Let’s create a brand new page ! Create a new file, for instance :
`sudo vim /var/www/html/info.php`
where we’ll write :
``` s
<?php
phpinfo();
?>
```

Now on your browser, if you go to page http://127.0.0.1/info.php you will get an error message. That’s because you need the fast-cgi protocol mentionned earlier :
`sudo lighty-enable-mod fastcgi`
`sudo lighty-enable-mod fastcgi-php`
`sudo service lighttpd force-reload`

We’re all good with lighttpd !

Now let’s move on to MariaDB :
`sudo apt install mariadb-server`
`sudo systemctl start mariadb`
`sudo systemctl enable mariadb`
`systemctl status mariadb`

MariaDB is installed, but it still needs some secure setting up. Please keep in mind that the root user for mariaDB is different from your VM root user :
`sudo mysql_secure_installation`

``` s
Enter current password for root (enter for none): <Enter>
Switch to unix_socket authentication [Y/n]: Y
Set(ou change) root password? [Y/n]: Y
New password: Iamgroot!
Re-enter new password: Iamgroot!
Remove anonymous users? [Y/n]: Y
Disallow root login remotely? [Y/n]: Y
Remove test database and access to it? [Y/n]:  Y
Reload privilege tables now? [Y/n]:  Y
```

Then restart mariadb :
`sudo systemctl restart mariadb`

We can now install a database :
`mysql -u root -p`

```s
MariaDB [(none)]> CREATE DATABASE wordpress_db;
MariaDB [(none)]> CREATE USER 'admin'@'localhost' IDENTIFIED BY 'WPpassw0rd';
MariaDB [(none)]> GRANT ALL ON wordpress_db.* TO 'admin'@'localhost' IDENTIFIED BY 'WPpassw0rd' WITH GRANT OPTION;
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;
```

If you execute again the earlier command :
`mysql -u root -p`

Then :
```s
MariaDB [(none)]> show databases;
```

You should get :
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| performance_schema |
| wordpress_db       |
+--------------------+

We’ve made sure that our new database is indeed here, we can now `EXIT`
If we want to delete a database, we we’ll execute :
`mysql -u root -p`

```s
MariaDB [(none)]> DROP DATABASE <db_name>;
```

If it’s a user we want to delete, then :
`mysql -u root -p`

```s
MariaDB [(none)]> SELECT User FROM mysql.user;
MariaDB [(none)]> DROP USER <user>;
```

Bear in mind : the username is the full name that was used at the time it was created. For instance, here it would be 'admin'@'localhost'

### Installing wordpress

First we need to install the wget and tar packages, so that we can work with archive files :
`sudo apt install wget`
`sudo apt install tar`

Now we can retrieve the latest official wordpress archive and install it :
`wget http://wordpress.org/latest.tar.gz`
`tar -xzvf latest.tar.gz`
`sudo mv wordpress/* /var/www/html/`
(if you can’t mv it, just cp it int the right path then rm the wordpress/* file)
`rm -rf latest.tar.gz wordpress/`

We want to keep a copy of the original config file, just in case…  so let’s rename and edit it
`sudo cp /var/www/html/wp-config-sample.php /var/www/html/wp-config.php`
`sudo vim /var/www/html/wp-config.php`

What we want to write is :

```s
<?php
/* ... */
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** Database username */
define( 'DB_USER', 'admin' );

/** Database password */
define( 'DB_PASSWORD', 'WPpassw0rd' );

/** Database host */
define( 'DB_HOST', 'localhost' );
```

You can either just copy and paste the previous line to only have those, or just amend the file to add them. Oh btw, if you know some html : no, you don’t need to close the `<?php`
The last step is to change the wordpress file permissions for the user www-data (that’s our web server), and restart lighttpd :
`sudo chown -R www-data:www-data /var/www/html/`
`sudo chmod -R 755 /var/www/html/`
`sudo systemctl restart lighttpd`


If you go back to your web browser and try again to connect to http://127.0.0.1:8080 you will now get to a wordpress configuration page ! Be aware however that you can only access it with a GUI...

### final config and cli use

We have now landed on that wordpress configuration page, let’s set it up. Obvously, you can use whatever title/name/password/email you like, including but not limited to your 42/VM credentials. Also, feel free to use the following lines to input your info, so that you can keep a copy of the info :

select language
site title : 
username:
Password :
Email : 

(And check the box to disallow indexing)

If you want to use wordpress from command line (and be able to edit your wordpress site from the VM !) you need to install wp-cli with curl :
`curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar`

`php wp-cli.phar --info`

Make sure the file is executable :
`sudo chmod 755 wp-cli.phar`

In order to make it globally available on the system, move it to /usr/local/bin and rename it as wp, then check the version :
`sudo mv wp-cli.phar /usr/local/bin/wp`
`wp cli version`
(should be at least 2.6)

To perform any operation on your wordpress, you need to be in the /var/www/html directory.
To create, edit and publish a post all in one line :
`wp post create --post_status=publish --post_title="<post title>" --edit`

Some explanations :
→ the `--post_status` flag set to publish means that you post will be published immediately after you have edited it. If you just want to draft it, set the status to `draft` instead of `publish`.
→ the `--post_title` flag is, well… how you want to name your post. For instance, you may want to name it « Hello world » : `--post_title="Hello world"`
→ the `--edit` command launches your favorite text editor (say, vim) so you can edit the body of your post
→ for more info and flags, type `wp help post create` (as you would use the linux man)
You can use html formatting, however it will only show when you read the full post, not in the homepage.
To print the post list, use :
`wp post list`

To delete a post, you need it’s ID, which you saw with the prevous command. Then :
`wp post delete <ID>`


And voila ! You have a fully functional wordpress, using lighttpd and mariaDB.

## Fail2ban

This package is useful for banning bruteforce attacks. Basically, if an IP attempts too many connections in too little time with the wrong login info, it will get banned (blacklisted) for a certain amount of time. The number of attempts, the timeframe and the jail time can all be fully configured to your liking.

Install Fail2ban :
`sudo apt install fail2ban`
`sudo systemctl start fail2ban`
`sudo systemctl enable fail2ban`
`sudo systemctl status fail2ban`

Let’s make a copy of the default config files `/etc/fail2ban/fail2ban.conf` and `/etc/fail2ban/jail.conf` so we can edit the copies. It is especially advisable to do so as an update to the package may remove these files, then you would have nothing to start from. These files include the basic config, and you can add more config info with the /etc/fail2ban/jail.d file.
As the `/etc/fail2ban/jail.conf`  must be only kept as reference and documentation, let’s create the `/etc/fail2ban/jail.local` file :
`sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local`
`sudo vim /etc/fail2ban/jail.local`

Amend the ssh section (around line 280) :

```s
[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
# mode   = normal
enabled  = true
maxretry = 3
findtime = 10m
bantime  = 1d
port     = 4242
logpath  = %(sshd_log)s
backend  = %(sshd_backend)s
```

Now, to monitor the connection attempts :
`sudo fail2ban-client status`
`sudo fail2ban-client status sshd`
`sudo tail -f /var/log/fail2ban.log`

If you want to check if it works, change the ban time to 1 minute, try connecting to the VM from the host machine with a wrong password, than check with :
`sudo fail2ban-client status sshd`

## FTP server

There are several ftp packages, we will use vsftp because it is :
- fully compatible CLI
- made for linux
- open source

### Install and configure the FTP server

First, install FTP with : 
`sudo apt install ftp`

Then vsftpd :
`sudo apt install vsftpd`

Check whether vsftpd was successfully installed :
`dpkg -l | grep vsftpd.`

You can now start and enable it :
`sudo systemctl start vsftpd`
`sudo systemctl enable vsftpd`

Once again, copy the config file so you have the original as a backup, then open it :
`sudo cp /etc/vsftpd.conf /etc/vsftpd.conf.orig`
`sudo vim /etc/vsftpd.conf`

Allow incoming connections using port 21/tcp (which is the default port for ftp) and port 20/tcp :
`sudo ufw allow 21/tcp`
`sudo ufw allow 20/tcp`

You also want to enable a range of passive ports :
`sudo ufw allow 40000:50000/tcp`

Some info on FTP default ports : 
→ 21 is used for management : it opens the server and sends commands to it
→ 20 is used for data transfer : it’s mainly needed for active mode

To set root folder for the FTP-connected user to `/home/<username>/ftp` (so that anyone logged onto your machine can access it) use following command lines:
`sudo mkdir /home/<username>/ftp`
`sudo chown nobody:nogroup /home/<username>/ftp`
`sudo chmod a-w /home/<username>/ftp`

Let’s create the folder for file uploads :
`sudo mkdir /home/<username>/ftp/files`

You may want to assign ownership to the user :
`sudo chown <username>:<username> /home/<username>/ftp/files`

Now let’s edit the configurations file. To enable any form of FTP write command, uncomment below line:
`31 #write_enable=YES`

Then add the following 5 lines anywhere in the file :

```s
user_sub_token=$USER
local_root=/home/$USER/ftp
userlist_enable=YES
userlist_file=/etc/vsftpd.userlist
userlist_deny=NO
```

Then you need to explicit a port range :

```s
pasv_min_port=40000
pasv_max_port=50000
```

To prevent user from accessing files or using commands outside the directory tree,  you need to uncomment below line (should be #114):
`#chroot_local_user=YES`

Now we can close the config file.

To whitelist FTP, use below commands :
`sudo touch /etc/vsftpd.userlist`
`echo <username> | sudo tee -a /etc/vsftpd.userlist`

Also, make sure that the user belongs to ftp group :
`sudo usermod -aG ftp <user>`


Restart the daemon :
`sudo systemctl restart vsftpd`

And make sure vsftpf is active :
`systemctl status vsftpd`

### Connecting the server

On your VM, FTP into your virtual machine with :
`ftp <ip-address>`

On your host machine, use :
`ftp -p <ip-address>`

For this, you need to use your VM login info.

FTP stands for File Transfer Protocol, so let's do some transfers.
To download a file into your local folder :
`get <file>`

And to upload a file to the server :
`put <file>`

You can terminate your FTP session at any time via CTRL + D. or `exit` or `bye`

## Aaaand...

We’re done with this project ! I also wanted to create an email server, but we only need 1 additional service on Born2beroot and I gave you 2. Maybe one day I’ll come back to this…
Until then, have fun !