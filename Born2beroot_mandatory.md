# Born2beroot


This is about the Born2beroot project for Ecole 42. The goal is to set up a virtual machine, using either Debian OS or CentOS. While discussing with my peers, I have come to realize that I did a few things using different process, so I thought I would share them here. 

The idea here is neither to explain in depth the installation and setup process, nor to give you with the answers you should provide for the evaluation, as there are various online ressources for that. What I want to do here is share some info/advice that prove very valuable to me, and that were scattered all over the internet.

By the way, first piece of advice : you should have your VM on a separate drive (flashdrive for instance) instead of the school server so that you won’t be affected by any server issue, additionnally you won’t have to be at school to work on the project and can do it anywhere.


## Install Debian on a VM

### Setting up the VM

The first step is to set up your virtual machine with VirtuallBox : start with some basic settings, allocate RAM and create disk space.
VM name : *no specific obligation, I just named it « Born2beroot »*
Machine Folder: *we have access to limited personal storage at school : you may use your dedicated sgoinfre although I strongly suggest to put it on a personal flash drive… also server issues may happen occasionnally… sooo just put it on you preferred external storage (I’m almost begging you here)
Type : linux
Version : Debian 64-bit

Then you allocate RAM, the recommended 1024MB setting is fine.

Next, create the hard disk now :you want to set it as VDI, and dynamically allocated.
The recommended size is 8GB which is perfectly fine – you definitely don’t need the 30GB mentionned in the subject screenshot if you stick to the mandatory part of the project. For instance, I did all the bonus and used only about 2GB (but would advise to set 4GB initially).

The next step is to install the VM. On VirtualBox, select your VM and start it.

The start-up disk you want to select is the OS .iso file. For Debian you mainly have a choice between net-install and CD/DVD install. I would recommend to avoid the latter because A) it’s much much heavier, and B) you will most likely need to edit some configuration files after installing it, in order to update your sources.
Then you want to select : Install – definitely make sure to NOT select « Graphical install », as per subject rules.
Local settings are to be set according to your preference and don’t matter much, although I would advise you to go for English when it comes to lamguage selection.
When configuring th network, enter the following :
Hostname : (intra login + 42, or whatever the subject says)
Domain name: (leave blank)
Root password : (make sure to set a strong one. In order to « show password in clear », selct the line and hit the space bar)
Full name for the new user : (you can either leave it blank or use your username)
Username :  (intra login or whatever the subject says)
Password for the new user : (same as root password)

We’re done for the settings, let’smove on to the partitionning setup.

### Setting up partitions

The subject asks for at least the following : boot, root, home and swap. For the bonus part, you want to also have var, var/log (yes as a separate partition from var), srv and tmp. We also want to set up encryption. So now we’re on the « partition disks » screen, and we will choose the following options/settings:
Manual → SCSIx (x is a number, so could be SCSI2 for instance) (sda) … HARDDISK
	└> « pri/log … FREE SPACE» → create a new partition (will do it twice, first time for boot then for the rest of the disk space)
		└> 500MB → primary → beginning → select mount point : boot → done setting up the partition
		└> (keep whatever size is left)→ logical → select mount point :« do not mount it » → done setting up the partition
	└> configure encrypted volumes→ « write the changes to disk and configure encrypted volume » : yes  → « create encrypted volume » → select “SDA5” → “done setting up the partition” → « finish » → « really erase the data » : yes
		Then you will be prompted to choose a passphrase for the encryption
	└> configure the Logical Volume Manager →Write changes ? yes
		└> create volume group → “LVMGroup”→ « /sda5_crypt »
		└> create logical volume → LVGMGroup
			└> root → (best to give it at least 2GB for peace of mind)
			└> (rinse and repeat with swap, home, var, srv, tmp, var-log choosing size accordingly. And yes, it’s var-log with only one dash)
		└> finish
	└> (select each LV – the line that starts with #)
		└> use as :EXT4 → mount point : (choose accordingly)
				└> for var/log, set mount point as : “enter manually” → /var/log
		└> for swap : use as:swap
	└> « finish partitioning and write changes to disk »

Finally, when installing the base system : 
- you don’t need to scan to configure package manager
- the proxy field can be left blank
- you don’t need to participate in the stat study
- when you get to the software selction screen, make sure to uncheck everything
- you DO WANT to install the GRUB boot loader on /sda

And now we’re done with the installation process !

## Mandatory settings


I will start with a little piece of advice : take frequent snapshots of your VM – say, after you’re done with a big chunk of configuration, or every X hours, whatever suits you. If anything goes wrong, you’ll have something you can revert to...
Even though they are not required for this project, you may find useful to get the linux man pages, and vim (or any other editor other than nano, which comes pre-installed) :
`sudo apt install man-db`
`sudo apt install vim`

### sudo

To install sudo, you must be logged as root. If you’re already logged in, just type :
`su root`		or		`su -`

`apt update && apt upgrade`
`apt install sudo`

Then you need to include root in sudo group, as well as your username :
`sudo usermod -aG sudo <username>`

Check which usernames are included in sudo group (or any other group):
`getent group <group>`

You can also check that root is included in sudo by being logged in as your username, and :
`sudo whoami`
The answer must be `root`.

To select your text editor:
`sudo update-alternatives --config editor`

For Vim, you want to choose `vim basic` over `vim tiny`. If it’s not there you may want to (re)install it.

Sudo settings are :
- custom message when entering wrong password
- 3 attempts max
- sudo commands log

Let’s start with the log. Create the following directory :
`var/log/sudo/sudo.log`

Then open sudoers file in secure mode :
`sudo visudo`

Add the following lines (some may already be present in some form) :
``` s
Defaults	requiretty
Defaults	env_reset
Defaults	passprompt=“<message>”
Defaults	insults // choisir : soit insults, soit badpass_message
Defaults	badpass_message=“<message>”
Defaults	logfile=“/var/log/sudo/sudo.log”
Defaults	passwd_tries=3
Defaults	secure_path=“/usr/local/sbin:/local/bin” //(etc)
```

Some infos:
`requiretty` -> tty=teletypewriter → prints the tty you’re using. By requiring it it means you can only use sudo commands from the actual machine and nor with some remote daemon
`env_reset` -> resets the terminal environment to remove any user variables. This is a safety measure used to clear potentially harmful environmental variables from the sudo session.
`badpass_message=“<message>”` → choose what the machine says in case of wrong password
`logfile=“/var/log/sudo/sudo.log”` → for the sudo log address
`passwd_tries=3` → max number of password attempts
`secure_path=“/usr/local/sbin:/local/bin”` → specifies the PATH (the places in the filesystem the operating system will look for applications) that will be used for sudo operations, so as to avoid potentially harmful PATHs
`mail_badpass` → you’ll have this one by default, it’s not needed for Born2beroot so feel free to remove it. What it does is mail bad sudo password attempts to the configured email address. By default it will be the root account.

Want some more ? You can try these settings :
`Defaults	passprompt=“<message>”`  //  customizes the password prompt message
`Defaults	insults` // you don’t know what to put as badpass_message ? Use this line instead, so the machine can tell you how lame you are for not being able to enter a password. If you have both `badpass_message` and `insults`, the latter overrides the former

### Network access and security

#### Access control and firewall

Check AppArmor status:
`sudo aa-status`

#### Install UFW

`sudo apt install ufw`
`sudo ufw enable`

Implement rules to allow or deny port 4242 :
`sudo ufw allow 4242`
`sudo ufw deny 4242`

Remove the rule you implemented
`sudo ufw delete allow 4242`
`sudo ufw delete deny 4242`

#### Install ssh

`sudo apt install openssh-server`

Check if ssh service is running
`sudo systemctl status ssh`

Change ssh standard port (from 22 to 4242) and prevent connection with root account:
`sudo vim /etc/ssh/sshd_config`
	└> uncomment the line related to the port and change it to 4242
	└> find the line PermitRootLogin, change prohibit-password with 	`no`
`sudo systemctl restart ssh`
`sudo ufw allow ssh`
`sudo ufw allow 4242`

Last, make sure to update the VM network settings on VirtualBox, and set port forwarding as TCP on ports 4242/4242. Though you want to make sure the 4242 port is available on the host machine. If it’s not, you just want to use another – unused – port, for instance 4243/4242.

To ssh into the VM you want to use the following command

`ssh acloos@127.0.0.1 -p <open port on host machine>`

Note that you can substitute `127.0.0.1` with `localhost`

To end the connexion you just need to type exit

### Password rules

#### Password settings

    • Min 10 characters, including at least one uppercase letter, one lowercase letter and one digit
    • max 3 identica characters consecutively
    • cannot include login
    • a new password must have at least 7 different characters from the previous one
    • all roots appliy to root account

`sudo apt install libpam-pwquality`
`sudo vim /etc/pam.d/common-password`

Find the following line
`password pam_unix.so`

Make sure it includes either `sha512` or `yescript` and add :
`minlen=10`

Then find the following line:
`password requisite pam_pwquality.so retry=3`

And add :
`ucredit=-1 lcredit=-1 dcredit=-1 maxrepeat=3 reject_username difok=7 enforce_for_root`

#### Aging control

    • Expires after 30 days
    • minimum 2 days between renewal
    • expiry warning 7 days rior

You want to edit the following file:
`sudo vim /etc/login.defs`

And edit is as follows (these lines are far ahead int the file, use you search tool) :

``` s
Pass maxdays		30
Pass mindays		2
Pass warn age		7
```

Warning: these new rules will not be automatically applied to existing users. For each existing users (which should be root and our own username, you need to use the ) you need to do this:

``` s
sudo chage -M 30 <user>
sudo chage -m 2 <user>
sudo chage -W 7 <user>
sudo chage -l <user>
```

And now reboot :
`sudo systemctl reboot`

### System administration

#### Hostname

You can change your hostname either by a command or by modifying a text file :
`sudo hostnamectl set-hostname <new>`
`sudo vim /etc/hostname`

The change may not appear on the console yet, you need to reboot for that (and you may get a conflict warning). If you don’t want to reboot, you can still check that the change is effective :
`hostnamectl status`


#### Adding users and groups

There are two commands to create a new user. The most basic one is `useradd`, the other one is `adduser`. The first one will ONLY create a username, you need to configure everything else (password, /home directory, etc) manually. With the second one, you will be prompted to answer several questions, including but not limited to : password, personal info...
The syntax is the same for both:
`sudo adduser <user>`
`sudo useradd <user>`

For this project, you only need a username and a password, there’s no need for a /home directory. If you use `adduser`, you can skip all questions but the one asking for a password. If you go for `useradd`, you can configure a password with :
`sudo passwd <user>`


To create a new group, just use :
`sudo groupadd <group>`

Remember how you added your own user to the sudo group earlier ? Well that’s how you add any user to any group :
`sudo usermod -aG <group> <user>`

If you want to check who belongs to a specific group :
`sudo getent group <group>`

## SCRIPT

### monitoring.sh

``` bash
#! /bin/bash

ARCH=$(uname -a)
SOCK=$(lscpu | egrep "Socket" | awk '{print $2}')
CORE=$(lscpu | egrep "Core" | awk '{if ($4) {print $4; exit}}')
PCPU=($SOCK * $CORE)
THREAD=$(lscpu | egrep "Core" | awk '{print $4}')
VCPU=($PCPU * $THREAD)
MUSE=$(free -th --mega | egrep "Total" | awk '{print $3}')
MTOT=$(free -th --mega | egrep "Total" | awk '{print $2}')
MPCT=$(free -th --mega | egrep "Total" | awk '{printf("%.2f%%"), $3 / $2}')
DUSE=$(df -h --total | egrep "total" | awk '{print $3}')
DTOT=$(df -h --total | egrep "total" | awk '{print $2}')
DPCT=$(df -h --total | egrep "total" | awk '{print $5}')
LOAD=$(top -bn1 | egrep ‘^%Cpu’ | awk '{printf "%.2f%%", $3 + $4}')
LBOOT=$(who | egrep "tty1" |sudo systec awk '{print $3 " " $4}')
LVMUSE=$(lsblk | egrep "lvm" | awk '{if ($1) {print "yes"; exit;} else {print "no"}}')
TCP=$(grep TCP /proc/net/sockstat | awk '{print $3}')
ULOG=$(who | wc -l)
IP=$(hostname -I | awk '{print $1}')
MAC=$(ip link show | egrep link/ether |awk '{print $2}')
SUDOLOG=$(sudo egrep COMMAND /var/log/sudo/sudo.log | wc -l)

wall -n "
________________
Architecture : $ARCH
CPU Physical : $PCPU
vCPU : $VCPU
Memory usage : $MUSE/$MTOT ($MPCT)
Disk usage : $DUSE/$DTOT ($DPCT)
CPU load : $LOAD
Last boot : $LBOOT
LVM use : $LVMUSE
TCP connections : $TCP established
User log : $ULOG
Network : $IP ($MAC)
Sudo : $SUDOLOG command(s) have been used
___________________________________________________"
```

### Setting up the script

When it comes to configuring the script, the subject hints at using cron. However this is not at all mandatory. The idea here is to have an underlying active process that will show up on the screen at a given frequency. So it made me think of the (in)famous systemd, and I wondered if I could use it here...
Disclaimer :
This is a VM, so one of its purposes is to tinker around without fear of breaking your own system. I am in no way an expert as to how an OS works « under the hood », just thought I would play around a little – and it worked beautifully ! Unless you know what you’re doing, I would not recommend playing with your « actual » physical computer…
Also, did you take any snapshot like I advised you to do when we started ? Well, now would be a goot time to take one – again, just in case (a backup never hurts anyway).

I hope I did not scare you too much about systemd, like I said it worked perfectly fine with me.

To summarize what we will do : 
- create the monitoring.sh script file in the appropriate directory
- got to the appropriate directory, and create a systemd service file
- in that same directory, create the systemd timer file

The script file contains what needs to appear on the screen (thanks to the `wall` command we included), the service file tells systemd what to do with it (i.e. display the monitoring info), the timer file tells systemd when to do that.

So where do we put all these files ? Well, I may have lied a tiny bit, I actually encountered one issue : the script was being executed on my user session only. The info I had gathered was telling me to place my script file in `/usr/bin`, and the other files in `/usr/lib/systemd/user` … you get why I had that problem… 
Ok, let’s start with the script itself : in order to be executed for all users, you want it to be in `/root` so get it there however you like (using ssh to copy the file to your user’s /home may be one good starting point, setting up your ftp server may be useful as well). You also want it to be executable and to belong to root, so chmod/chown if necessary :
`chmod 755 <monitoring.sh>`
`chown root /root/monitoring.sh`

#### Setting up with systemd

Like I said, we will need two different files, and for them to work with all users we need them both in /etc/systemd/system
These files will be :
monitoring.service
monitoring.timer
When creaing a systemd service, make sure the name is not used by any other service or it will cause some problems...
Lets’s start with the monitoring.service file and how we want to write it :

``` s
[Unit]
Description=A job to monitor computer use #just give a short and precise description
[Service]
Type=simple
ExecStart=/root/monitoring.sh

[Install]
WantedBy=default.target
```

The « unit » part is mainly about description and metadata, the « service » part explains how to execute it, the « install » part defines how systemd will handle the service unit. If you want more detailed info on the various options, just start with the man : systemd.unit(5) and systemd.service(5)

Now for the monitoring.timer :

``` s
[Unit]
Description=Schedule a message every 10 minutes
RefuseManualStart=no  # Allow manual starts
RefuseManualStop=no   # Allow manual stops

[Timer]
#Does not execute job if it missed a run due to machine being off
Persistent=false
#Run 600 seconds after boot for the first time
OnBootSec=600
#Run every 10 minutes thereafter
OnUnitActiveSec=600
#File describing job to execute
Unit=monitoring.service

[Install]
WantedBy=timers.target
```

For more info on the timer options, check the man : systemd.timer(5)


Now that both files are created, we can launch the service. Remember that we created two files, so when you act on the service, you need to act on both files :
`sudo systemctl enable monitoring.service`
`sudo systemctl start monitoring.service`
`sudo systemctl enable monitoring.timer`
`sudo systemctl start monitoring.timer`

With the same syntax, you can also stop or disable the service.

Below are a few more commands to check or reload the service:
`sudo systemctl status monitoring`
`sudo systemctl list-unit-files`
`sudo systemctl reset-failed`
`sudo systemctl daemon-reload`

#### Setting up with cron

Just like with systemd, the script needs to belong to root, be executable, and placed as :
`/root/monitoring.sh`

You want to enable it at boot:
`systemctl enable cron`

To edit the cron job file:
`crontab -e`

To show the script every 10 minutes:
`*/10 * * * * bash /root/monitoring.sh`

However if you leave it like this, it will start on boot. For this project we need for it to wait 10 minutes before the 1st execution. To achieve that, we need a 2nd script: sleep.sh
This one too needs to belong to root and be executable and be in the same directory.

See sleep.sh script below:

``` bash
#!bin/bash

# Get boot time minutes and seconds
BOOT_MIN=$(uptime -s | cut -d ":" -f 2)
BOOT_SEC=$(uptime -s | cut -d ":" -f 3)

# Calculate number of seconds between the nearest 10th minute of the hour and boot time:
# Ex: if boot time was 11:43:36
# 43 % 10 = 3 minutes since 40th minute of the hour
# 3 * 60 = 180 seconds since 40th minute of the hour
# 180 + 36 = 216 seconds between nearest 10th minute of the hour and boot
DELAY=$(bc <<< $BOOT_MIN%10*60+$BOOT_SEC)

# Wait that number of seconds
sleep $DELAY*/10 * * * * bash /root/monitoring.sh
```

That’s it for the sleep script.
Now you need to edit cron job to include the sleep script:
`*/10 * * * * bash /root/sleep.sh && bash /root/monitoring.sh`

You can stop the service with:
`sudo systemctl stop cron`

then restart it:
`sudo systemctl start cron`
`sudo systemctl enable cron`
`sudo systemctl status cron`



We're done with the mandatory part of the project!

## References

These are websites that proved useful to me while working on this project.

https://www.codequoi.com/en/born2beroot-01-creating-a-debian-virtual-machine/
https://baigal.medium.com/born2beroot-e6e26dfb50ac
https://thishosting.rocks/install-php-on-ubuntu/
https://crontab.pro
https://www.techrepublic.com/article/controlling-passwords-with-pam/
https://www.digitalocean.com/community/tutorials/how-to-use-wp-cli-v2-to-manage-your-wordpress-site-from-the-command-line
https://doc.ubuntu-fr.org/fail2ban
http://vsftpd.beasts.org/vsftpd_conf.html
https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-a-user-s-directory-on-debian-10