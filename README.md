# Born2beroot


## Description
Born2beroot is one of the early projects in 42 cursus. The goal here is to gain basic sysadmin skills.
I will explain here how I worked on this project, and how I got a score of 125, as well as the "outstanding project" flag - yes I'm bragging a little.

The mandatory part includes partitiong and LVM, user/group management, and basic security (password, etc).
We are also asked to create a small script to display the system activity at specific times/intervals, and advised to use wall and cron.
Well, I didn't like cron much, and figured I would rather delve into systemd.
So if you are a 42 student gathering info on Born2beroot, be aware that I am using a different method. It is is just as valid and you won't be in trouble, but be careful with it as it touches the core of of your system.

The bonus part asks for 3 different things:
1. partition your system in a specific way, which you actually want to do when you install the system.
2. set up a basic wordpress, using lighttpd and mariaDB. You must not use Apache or Nginx.
3. set up any other service of your choice, and explain why/how you dit it. 
		I actually set up 2 other services : fail2ban, and an ftp server

It was quite fun for me, and I loved trying to find all sorts of alternate solutions to the various issues.
I hope you will enjoy this project as much as I did !