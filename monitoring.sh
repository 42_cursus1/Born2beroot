#! /bin/bash

ARCH=$(uname -a)
SOCK=$(lscpu | egrep "Socket" | awk '{print $2}')
CORE=$(lscpu | egrep "Core" | awk '{if ($4) {print $4; exit}}')
PCPU=($SOCK * $CORE)
THREAD=$(lscpu | egrep "Core" | awk '{print $4}')
VCPU=($PCPU * $THREAD)
MUSE=$(free -th --mega | egrep "Total" | awk '{print $3}')
MTOT=$(free -th --mega | egrep "Total" | awk '{print $2}')
MPCT=$(free -th --mega | egrep "Total" | awk '{printf("%.2f%%"), $3 / $2 * 100}')
DUSE=$(df -h --total | egrep "total" | awk '{print $3}')
DTOT=$(df -h --total | egrep "total" | awk '{print $2}')
DPCT=$(df -h --total | egrep "total" | awk '{print $5}')
LOAD=$(top -bn1 | egrep '^%Cpu' | awk '{printf "%.2f%%", $3 + $4}')
LBOOT=$(who | awk '{print $3 " " $4}')
LVMUSE=$(lsblk | egrep "lvm" | awk '{if ($1) {print "yes"; exit;} else {print "no"}}')
TCP=$(grep TCP /proc/net/sockstat | awk '{print $3}')
ULOG=$(who | wc -l)
IP=$(hostname -I | awk '{print $1}')
MAC=$(ip link show | egrep link/ether |awk '{print $2}')
SUDOLOG=$(sudo egrep COMMAND /var/log/sudo/sudo.log | wc -l)

wall "
________________
Architecture	: $ARCH
CPU Physical	: $PCPU
vCPU		: $VCPU
Memory usage	: $MUSE/$MTOT ($MPCT)
Disk usage	: $DUSE/$DTOT ($DPCT)
CPU load	: $LOAD
Last boot	: $LBOOT
LVM use		: $LVMUSE
TCP connections	: $TCP established
User log	: $ULOG
Network		: $IP ($MAC)
Sudo		: $SUDOLOG command(s) have been used
___________________________________________________"

